/*
 * Copyright (C) 2022  Daniel Frost
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * nogreencamera is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.12
import Ubuntu.Components 1.3
import io.thp.pyotherside 1.4 //for Python
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: 'nogreencamera.danfro'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('noGreenCamera')

            Label {
              id: versionText
              anchors {right: parent.right; rightMargin: units.gu(2); verticalCenter: parent.verticalCenter}
              font.italic: true
              font.weight: Font.Light
              fontSize: "normal"
              text: i18n.tr("Version %1").arg(Qt.application.version)
            }
        }

        Flickable {
            id: mainPageFlickable
            clip: true
            flickableDirection: Flickable.AutoFlickIfNeeded

            anchors {
                topMargin: units.gu(2)
                bottomMargin: units.gu(1)
                top: header.bottom
                bottom: issueLink.top
                left: parent.left
                right: parent.right
            }

            contentHeight: pageContent.childrenRect.height + units.gu(2)

            ColumnLayout {
                id: pageContent
                anchors {
                    left: parent.left;
                    top: parent.top;
                    right: parent.right;
                    margins: units.gu(2)
                }
                spacing: units.gu(1.5)

                Label {
                    id: descriptionCaptionLabel
                    Layout.fillWidth: true
                    text: i18n.tr("App description")
                    font.bold: true
                    horizontalAlignment:  Text.AlignHCenter
                }
                Label {
                    id: descriptionReasonLabel
                    Layout.fillWidth: true
                    text: i18n.tr("On Xperia X the camera does occasionally display the upper half of the screen in green. Annoying!")
                    horizontalAlignment:  Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                Label {
                    id: descriptionSolutionLabel
                    Layout.fillWidth: true
                    text: i18n.tr("This can be fixed <b>temporarely</b> by applying the following terminal command:%1").arg("<br><i>setprop ctl.restart minimedia</i>")
                    horizontalAlignment:  Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                Label {
                    id: descriptionUnconfinedLabel
                    Layout.fillWidth: true
                    text: i18n.tr("This app does run this terminal comman via Python. For this reason the app is unconfined.")
                    horizontalAlignment:  Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                Label {
                    id: warningCameraAppLabel
                    Layout.fillWidth: true
                    text: i18n.tr("Close camera app before pressing the button. Otherwise camera app may break.")
                    color: theme.palette.normal.negative
                    horizontalAlignment:  Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                Label {
                    id: warningTrustStoreLabel
                    Layout.fillWidth: true
                    text: i18n.tr("The command causes trust-store to work at over 150% CPU power. So it will eat battery up quickly.\n => Take your photos and then reboot...")
                    color: theme.palette.normal.negative
                    horizontalAlignment:  Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
                Button{
                    Layout.minimumHeight: units.gu(10)
                    text: " " + i18n.tr("Press here to:\n 1. run that terminal command\n 2. open camera app\n 3. automatically close this app")
                    color: theme.palette.normal.activity
                    Layout.alignment: Qt.AlignHCenter
                    onClicked: {
                        py.call('os.system', ["setprop ctl.restart minimedia"], function (result) {
                                                      //TODO: add error handling
                                                  });
                        doneLabel.visible = true
                        Qt.openUrlExternally("appid://com.ubuntu.camera/camera/current-user-version")
                        quitTimer.start()
                    }
                }
                Label {
                    id: doneLabel
                    visible: false
                    Layout.fillWidth: true
                    text: "\n\n" + i18n.tr("Done. Closing app now...")
                    font.bold: true
                    horizontalAlignment:  Text.AlignHCenter
                    wrapMode: Text.WordWrap
                }
            }
        }



        Row {
            id: issueLink
            anchors {
              bottom: parent.bottom;
              bottomMargin: units.gu(3);
              horizontalCenter: parent.horizontalCenter
            }
            Label {
                id: bugreport
                text: i18n.tr("Report an issue: ")
            }
            Label {
                id: buglink
                text: i18n.tr("noGreenCamera on GitLab")
                color: theme.palette.normal.activity
                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally('https://gitlab.com/Danfro/nogreencamera/issues')
                }
            }
        }
    }

    //initiate a Python component for calls to Python commands
    Python {
      id: py

      Component.onCompleted: {
          //this works as the import statement in a python script
          importModule('os', function() { console.log("DEBUG: python loaded"); });
      }
    }

    Timer {
        // close the app after a set time
        id: quitTimer
        interval: 500 // 1/2 second
        running: false
        repeat: false
        onTriggered: Qt.quit();
    }

}
