# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the nogreencamera.danfro package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: nogreencamera.danfro\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-07-22 14:52+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../qml/Main.qml:37 nogreencamera.desktop.in.h:1
msgid "noGreenCamera"
msgstr ""

#: ../qml/Main.qml:45
msgid "Version %1"
msgstr ""

#: ../qml/Main.qml:78
msgid "App description"
msgstr ""

#: ../qml/Main.qml:85
msgid ""
"On Xperia X the camera does occasionally display the upper half of the "
"screen in green. Annoying!"
msgstr ""

#: ../qml/Main.qml:92
msgid ""
"This can be fixed <b>temporarely</b> by applying the following terminal "
"command:%1"
msgstr ""

#: ../qml/Main.qml:99
msgid ""
"This app does run this terminal comman via Python. For this reason the app "
"is unconfined."
msgstr ""

#: ../qml/Main.qml:105
msgid ""
"Press here to:\n"
" 1. run that terminal command\n"
" 2. automatically close this app"
msgstr ""

#: ../qml/Main.qml:121
msgid "Done. Closing app now..."
msgstr ""

#: ../qml/Main.qml:140
msgid "Report an issue: "
msgstr ""

#: ../qml/Main.qml:144
msgid "noGreenCamera on GitLab"
msgstr ""
