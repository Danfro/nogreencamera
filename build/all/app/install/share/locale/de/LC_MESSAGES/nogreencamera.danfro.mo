��    
      l      �       �      �        a     M   |     �  Y   �  R   6  
   �     �  ~  �     +     <  [   Z  ]   �       Z   *  R   �  
   �     �                  	             
              App description Done. Closing app now... On Xperia X the camera does occasionally display the upper half of the screen in green. Annoying! Press here to:
 1. run that terminal command
 2. automatically close this app Report an issue:  This app does run this terminal comman via Python. For this reason the app is unconfined. This can be fixed <b>temporarely</b> by applying the following terminal command:%1 Version %1 noGreenCamera on GitLab Project-Id-Version: nogreencamera.danfro
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2022-07-22 14:48+0000
PO-Revision-Date: 2022-07-22 16:49+0200
Last-Translator: Daniel Frost <one@frostinfo.de>
Language-Team: 
Language: de
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 3.0.1
 App Beschreibung Erledigt. App wird beendet... Am Xperia X wird bei der Kamera gelegentlich die obere Bildhälfte grün angezeigt. Nervig! Drücke hier um:
 1. diesen Terminalbefehl auszuführen
 2. die App automatisch zu schließen Einen Fehler melden:  Diese App führt diesen Befehl per Python aus. Deshalb ist sie unconfined (unbeschränkt). Dies kann <b>vorübergehend</b> behoben werden mit dem folgenden Terminalbefehl:%1 Version %1 noGreenCamera bei GitLab 