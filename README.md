# noGreenCamera

On Xperia X the camera does occasionally display the upper half of the screen in green. Annoying!
This can be fixed **temporarely** by applying the following terminal command: `setprop ctl.restart minimedia`
This app does run this terminal comman via Python. For this reason the app is unconfined.

Warning: Close camera app before pressing the button. Otherwise camera app may break.

Warning: The command may bring short term relief regarding the half green screen, but it causes trust-store to work at over 150% CPU power. So it will eat battery up quickly. => Take your photos and then reboot...

## License

Copyright (C) 2022  Daniel Frost

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
